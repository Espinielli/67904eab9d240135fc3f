**Arc Bubble Connection** Chart displaying graph data.

The data is a small sample of Lobbyist Activity from http://sfgov.org.

https://data.sfgov.org/City-Management-and-Ethics/Lobbyist-Activity-Contacts-of-Public-Officials/hr5m-xnxc


forked from <a href='http://bl.ocks.org/mattdh666/'>mattdh666</a>'s block: <a href='http://bl.ocks.org/mattdh666/2a7a7eecb9a75347241b'>Arc Bubble Connector Chart</a>